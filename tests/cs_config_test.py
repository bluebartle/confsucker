import os
import pathlib
import shutil
import unittest

import yaml

from confsucker.cs_config import ConfigFiles


class TestConfig(unittest.TestCase):
    """
    Test that we can load config and replacements files
    """

    conf_dir: str

    def setUp(self):
        self.conf_dir = 'config'

    def test_existing_config(self):
        """Test that we can load an existing config file"""
        config = {'confhole': {'path': 'some/path', 'root': 'some/root', 'ignore': ['dir', 'file'], 'vcs': 'none'}}
        file_path = self.conf_dir + '/config.yml'
        os.makedirs(self.conf_dir, exist_ok=True)
        with open(file_path, 'w') as conf_file:
            yaml.safe_dump(config, conf_file)

        confhole = ConfigFiles(self.conf_dir).config.confhole

        assert confhole.path == 'some/path'
        assert confhole.root == 'some/root'
        assert confhole.ignore == ['dir', 'file']
        assert confhole.vcs == 'none'

    def test_no_existing_config(self):
        """Test that a default configuration file is created if none exists"""
        home = str(pathlib.Path.home())
        confhole = ConfigFiles(self.conf_dir).config.confhole

        assert confhole.path == home + '/git/confhole'
        assert confhole.root == home
        assert confhole.ignore == ['.git', 'README.md']
        assert confhole.vcs == 'git'

    def test_existing_replacements(self):
        """Test that we can load an existing replacements file"""
        replacements = {'default': {'alpha': 'beta'}, 'some/path': {'alpha': 'gamma'}}
        file_path = self.conf_dir + '/replacements.yml'
        os.makedirs(self.conf_dir, exist_ok=True)
        with open(file_path, 'w') as replacements_file:
            yaml.safe_dump(replacements, replacements_file)

        loaded_replacements = ConfigFiles(self.conf_dir).replacements

        assert loaded_replacements.default == {'alpha': 'beta'}
        assert loaded_replacements.overrides('some/path') == {'alpha': 'gamma'}

    def test_no_existing_replacements(self):
        """Test that an empty replacements file is created if none exists"""
        default = ConfigFiles(self.conf_dir).replacements.default

        assert default == {'example_var': 'local_value'}

    def tearDown(self):
        shutil.rmtree(self.conf_dir)
