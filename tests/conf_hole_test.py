import os
import shutil
import unittest

from confsucker.conf_hole import ConfHole


class TestConfHole(unittest.TestCase):
    """
    Tests for ConfHole
    """
    conf_hole: ConfHole

    @classmethod
    def setUpClass(cls):
        conf_hole_path = 'confhole'
        dir_path = conf_hole_path + '/dir'
        child_dir_path = dir_path + '/child'

        cls.file_path = dir_path + '/file'
        cls.child_file_path = child_dir_path + '/file'

        os.mkdir(conf_hole_path)

        os.makedirs(child_dir_path)
        open(cls.file_path, 'a').close()
        open(cls.child_file_path, 'a').close()

        cls.conf_hole = ConfHole('', conf_hole_path)

    def test_rel_path(self):
        """Test that the relative path of files in the ConfHole are calculated"""
        rel_paths = map(self.conf_hole.rel_path, self.conf_hole.contents())
        assert self.file_path, self.child_file_path in rel_paths

    def test_contents(self):
        """Test that files are correctly listed"""
        assert os.path.abspath(self.file_path), os.path.abspath(self.child_file_path) in self.conf_hole.contents()

    def test_contents_file_name_ignored(self):
        """Test that files with an ignored file name are not listed"""
        assert not self.conf_hole.contents(['file'])

    def test_contents_dir_name_ignored(self):
        """Test that files which are children of an ignored directory are not listed"""
        assert not self.conf_hole.contents(['dir'])

    def test_contents_child_dir_name_ignored(self):
        """Test that files which are children of an ignored directory are not listed, but parents are"""
        contents = self.conf_hole.contents(['child'])
        assert os.path.abspath(self.file_path) in contents
        assert os.path.abspath(self.child_file_path) not in contents

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(cls.conf_hole.path)
