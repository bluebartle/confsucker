import os
import shutil
import unittest

from confsucker.conf_hole import ConfHole
from confsucker.sucker import Sucker


class TestSucker(unittest.TestCase):
    """
    Tests for Sucker
    """

    def __init__(self, method_name):
        super().__init__(method_name)

        self.conf_hole_path = 'confhole'
        self.suck_from = 'file'

    def setUp(self):
        os.mkdir(self.conf_hole_path)

    def test_suck_local(self):
        """Test sucking a file into a conf hole with root './'"""
        self.do_suck('./')

    def test_suck_global(self):
        """Test sucking a file into a conf hole with root '/'"""
        self.do_suck('/')

    def test_blow_local(self):
        """Test blowing a file from a conf hole with root './'"""
        self.do_blow('./')

    def test_blow_global(self):
        """Test blowing a file from a conf hole with root '/'"""
        self.do_blow('/')

    def tearDown(self):
        os.remove(self.suck_from)
        shutil.rmtree(self.conf_hole_path)

    def do_suck(self, root):
        suck_from_contents = 'beta'
        conf_hole = ConfHole(root, self.conf_hole_path)

        with open(self.suck_from, 'x') as suck_from_file:
            suck_from_file.write(suck_from_contents)

        suck_to = os.path.join(
            os.path.abspath(self.conf_hole_path),
            os.path.relpath(self.suck_from, root))

        sucker = Sucker(conf_hole, {'alpha': 'beta'})
        sucker.suck(self.suck_from)

        with open(suck_to) as suck_to_file:
            assert '$cs_alpha_$' in suck_to_file.readline()

    def do_blow(self, root):
        blow_from_contents = '$cs_alpha_$'
        conf_hole = ConfHole(root, self.conf_hole_path)

        blow_from = os.path.join(
            os.path.abspath(self.conf_hole_path),
            os.path.relpath(self.suck_from, root))

        os.makedirs(os.path.split(blow_from)[0], exist_ok=True)

        with open(blow_from, 'x') as blow_from_file:
            blow_from_file.write(blow_from_contents)

        sucker = Sucker(conf_hole, {'alpha': 'beta'})
        sucker.blow(blow_from)

        with open(self.suck_from) as blow_to_file:
            assert 'beta' in blow_to_file.readline()
