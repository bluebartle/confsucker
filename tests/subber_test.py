import os
import unittest

from confsucker.subber import Subber, UnmatchedVariableError


class TestSubber(unittest.TestCase):
    """
    Tests for Subber
    """

    def __init__(self, method_name):
        super().__init__(method_name)

        self.in_path = 'in'
        self.out_path = 'out'
        self.replacements = {'alpha': 'one', 'beta': 'two', 'gamma': 'three'}

    def test_subs(self):
        """Test substitution of variables in file copy"""
        with open(self.in_path, 'x') as inFile:
            inFile.write('$cs_alpha_$_$cs_beta_$_$cs_gamma_$')

        subber = Subber(self.replacements)
        subber.replace(self.in_path, self.out_path)

        with open(self.out_path) as outFile:
            assert 'one_two_three' in outFile.readline()

    def test_subs_unmatched(self):
        """Test that we get an error and do not copy if unmatched variables are found"""
        with open(self.in_path, 'x') as inFile:
            inFile.write('$cs_delta_$_$cs_beta_$_$cs_epsilon_$')

        subber = Subber(self.replacements)
        unmatched_vars = []

        try:
            subber.replace(self.in_path, self.out_path)
        except UnmatchedVariableError as error:
            unmatched_vars = error.variables

        assert 'delta', 'epsilon' in unmatched_vars
        assert not os.path.exists(self.out_path)

    def test_subs_inverted(self):
        """Test inverted substitution of variables in file copy"""
        with open(self.in_path, 'x') as inFile:
            inFile.write('one_two_three')

        subber = Subber(self.replacements)
        subber.replace(self.in_path, self.out_path, True)

        with open(self.out_path) as outFile:
            assert '$cs_alpha_$_$cs_beta_$_$cs_gamma_$' in outFile.readline()

    def tearDown(self):
        os.remove(self.in_path)
        try:
            os.remove(self.out_path)
        except FileNotFoundError:
            pass
