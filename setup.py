from setuptools import setup

setup(
    name='confsucker',
    version='0.1.0',
    description='VCS wrapper for config file management',
    author='bluebartle',
    author_email='ratskinmahoney@gmail.com',
    packages=['confsucker'],
    test_suite='tests',
    install_requires=[
        'GitPython',
        'PyYaml'
    ],
    scripts=[
        'scripts/conf-sucker'
    ]
)
