# confsucker

VCS wrapper for config file management. Safely manage configuration in git or any other VCS
or file synchronisation tool without exposing user names, passwords or other sensitive
details of your local system.

## What?

Basically this combines a simplified wrapper around git with some basic string substitution which allows you to locally define a bunch of strings that should be removed from files before being added to git. These strings are replaced with variables and are then substituted for the original values when pulling back into the file system. The fairly obvious result of which is that:

- You can push your config files, or dot files, or details of your extra-marital affairs to a shared remote repository without compromising yourself.
- You can manage a master copy of a config file and any variables, such as ip addresses and hostnames, can be managed locally with minimal fuss.

## How?

### Installation

Clone the repo and run `./install.sh` in the root (sudo may be required). Once installed run `conf-sucker` to create the config files.

### Configuration and Usage

There are two files to know about, and two commands. Lets start with the files...

#### .config/conf-sucker/config.yml

This is created on first use and allows you to specify:
- path - The location of the 'confhole': the local git repository in which your files will be managed.
- root - The location in your filesystem relative to which paths will be stored in the confhole. This is not especially important (yet), but your home directory is a sensible default.
- ignore - A list of strings which if matched by directory or file names should result in those directories or files being ignored in the confhole rather than copied back into your filesystem. This is used to ensure we don't do anything stupid with the '.git' directory, but you can add whatever you like to it.
- vcs - The VCS managing the confhole. The default is 'git'. Set to 'none' if you're not using git or if you just don't want conf-sucker managing your commits.

#### .config/conf-sucker/replacements.yml

This is also created on first use and allows you to specify a map of variable names to real values, such that the real values are substituted for the variable names when anything is added to the confhole, and vice versa when anything is pushed back out.

All mappings should be added under the 'default' heading. Overrides for specific files will be supported soon.


... and now the commands.

#### conf-sucker suck [path-to-file...]

Suck all specified files into the confhole (or re-suck all files already in the confhole if no files are specified), creating or updating managed copies after substituting out all values defined in replacements.yml. All changes are then committed and pushed if appropriate.

#### conf-sucker blow

Blow all files in the confhole back into the filesystem, pulling from the remote repository if appropriate before substituting back all the replaced values.

## Why?

The why of the functionality is pretty straightforward and covered above. The why of the project is 'because I wanted to learn python'.

## Project

The project is managed in [Trello](https://trello.com/b/9Kkf3a6A/confsucker). You're welcome to raise issues here if you wish.
