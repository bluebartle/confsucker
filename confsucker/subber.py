import re


class UnmatchedVariableError(Exception):
    """
    Error to be raised if variables are found for which there are no replacements.
    """
    variables: set

    def __init__(self, message, variables):
        super(UnmatchedVariableError, self).__init__(message)
        self.variables = set(variables)


class Subber:
    """
    Provides methods for substituting strings into or out of copied files
    """
    _replacements: dict

    def __init__(self, replacements):
        self._replacements = replacements if replacements else {}

    def replace(self, in_path, out_path, invert=False):
        """
        Copy in_path to out_path substituting strings as defined in replacements. An UnmatchedVariableError will
        be raised and copy aborted if any confsucker variables are undefined.
        :param in_path: path to copy from
        :param out_path: path to copy to
        :param invert: if True the replacements will be inverted, switching value and key
        """
        replacements = self._replacements.copy()
        replacements = {'$cs_' + k + '_$': v for k, v in replacements.items()}

        if invert:
            replacements = {v: k for k, v in replacements.items()}

        content = ''
        with open(in_path) as in_file:

            for line in in_file:
                for pattern, sub in replacements.items():
                    line = line.replace(pattern, sub)
                content += line

        if not invert:
            unmatched_vars = re.findall(r'\$cs_(.*?)_\$', content)
            if unmatched_vars:
                raise UnmatchedVariableError('Unmatched variables found in ' + out_path, unmatched_vars)

        with open(out_path, 'w') as out_file:
            out_file.write(content)
