import os
from typing import List, Union


class ConfHole:
    """
    A target into which files can be sucked.
    Consists of a path to suck to, and a root relative to which files are defined.
    """
    root: str
    path: str

    def __init__(self, root, path):
        self.root = os.path.abspath(root)
        self.path = os.path.abspath(path)

    def rel_path(self, abs_path):
        """
        Get the path of a file in the ConfHole relative to the ConfHole
        :param abs_path: absolute path of the file
        :return: relative path of the file
        """
        return os.path.relpath(abs_path, self.path)

    def contents(self, ignore=None):
        """
        List absolute paths to all files in the ConfHole
        :param ignore: list of directory and file names to ignore
        :return: list of paths
        """
        if ignore is None:
            ignore = []

        contents: List[Union[bytes, str]] = []

        for (dir_path, dir_names, file_names) in _filtered_walk(self.path, ignore):
            for file_name in file_names:
                contents.append(os.path.join(dir_path, file_name))

        return contents


def _filtered_walk(top, ignore=None):
    """
    Walk the directory top down from top,
    :param top: starting directory
    :param ignore: list of directory and file names to ignore
    :return: tuple of (dir_path, dir_names, file_names)
    """
    if ignore is None:
        ignore = []

    for dir_path, dir_names, file_names in os.walk(top):
        dir_names[:] = [d for d in dir_names if d not in ignore]
        file_names[:] = [f for f in file_names if f not in ignore]
        yield dir_path, dir_names, file_names
