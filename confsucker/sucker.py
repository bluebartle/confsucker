import os

from confsucker.conf_hole import ConfHole
from confsucker.subber import Subber


class Sucker:
    """
    Provides methods for sucking files from the filesystem into the ConfHole
    and blowing files from the ConfHole back to the filesystem.
    """
    _conf_hole: ConfHole
    _replacements: dict

    def __init__(self, conf_hole, replacements):
        self._conf_hole = conf_hole
        self._replacements = replacements

    def suck(self, in_path):
        """
        Suck a file from a path in the filesystem into the ConfHole
        :param in_path: path from which to suck
        """
        abs_path = os.path.abspath(in_path)
        relative_to_root = os.path.relpath(abs_path, self._conf_hole.root)

        sucked_path = os.path.join(self._conf_hole.path, relative_to_root)

        os.makedirs(os.path.split(sucked_path)[0], exist_ok=True)

        subber = Subber(self._replacements)
        subber.replace(abs_path, sucked_path, True)

    def blow(self, in_path):
        """
        Blow a file from the ConfHole into the filesystem. An UnmatchedVariableError will be raised if there are
        any variables present in the file for which there are no replacements.
        :param in_path: path from which to blow
        """
        abs_path = os.path.abspath(in_path)
        relative_to_root = os.path.relpath(abs_path, self._conf_hole.path)

        blow_path = os.path.join(self._conf_hole.root, relative_to_root)

        os.makedirs(os.path.split(blow_path)[0], exist_ok=True)

        subber = Subber(self._replacements)
        subber.replace(abs_path, blow_path)
