from git import Repo, UnmergedEntriesError

from confsucker.conf_hole import ConfHole
from confsucker.vcs_handler import VCSPushError


class Gitter:
    """
    Provide push and pull functions for git.

    Note: push and pull are intended to be easy to substitute for another VCS and so are simplified.
    E.g., push incorporates both commit and push to remote..
    """
    _conf_hole: ConfHole

    def __init__(self, conf_hole):
        self._conf_hole = conf_hole
        self.repo = Repo(conf_hole.path)

    def pull(self):
        """
        Pull from origin
        """
        if self.repo.remotes and self.repo.remotes.origin.exists():
            self.repo.remotes.origin.pull()

    def push(self):
        """
        Commit all local changes and push to origin
        """
        rel_paths = map(self._conf_hole.rel_path, self._conf_hole.contents(['.git']))

        self.repo.index.add(rel_paths)
        try:
            self.repo.index.commit('Committed by conf-sucker')
        except UnmergedEntriesError:
            print('  Commit failed')
            raise VCSPushError

        if self.repo.remotes and self.repo.remotes.origin.exists():
            push = self.repo.remotes.origin.push()
            for info in push:
                if info.flags & info.REJECTED:
                    print('  Push rejected. Summary:')
                    print('    ' + info.summary)
                    raise VCSPushError
