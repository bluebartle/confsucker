class VCSPushError(Exception):
    """Error raised if there is an issue pushing to VCS"""
    pass
