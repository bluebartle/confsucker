import os
import pathlib

import yaml


class HoleConf:
    path: str
    root: str
    ignore: list
    vcs: str

    def __init__(self, yml):
        self.path = yml.get('path')
        self.root = yml.get('root')
        self.ignore = yml.get('ignore')
        self.vcs = yml.get('vcs')


class Config:
    confhole: HoleConf

    def __init__(self, yml):
        self.confhole = HoleConf(yml.get('confhole'))


class Replacements:
    default: dict

    def __init__(self, yml):
        self._yml = yml
        self.default = yml.get('default')

    def overrides(self, path):
        return self._yml.get(path)


class ConfigFiles:
    """
    Wrapper for confsucker's config file and replacements file
    """
    _conf_dir: str
    _config: dict
    _replacements: dict

    def __init__(self, conf_dir):
        self._conf_dir = conf_dir
        self._config = {}
        self._replacements = {}

    @property
    def config(self):
        """
        Get the confsucker configuration file as a dict. Create the file with some defaults if it doesn't exist
        :return: Config
        """
        if not self._config:
            conf_file_path = os.path.join(self._conf_dir, 'config.yml')
            try:
                with open(conf_file_path, 'r') as conf_stream:
                    self._config = yaml.safe_load(conf_stream)
            except FileNotFoundError:
                os.makedirs(self._conf_dir, exist_ok=True)
                home = str(pathlib.Path.home())
                self._config = {
                    'confhole': {
                        'path': home + '/git/confhole', 'root': home, 'ignore': ['.git', 'README.md'], 'vcs': 'git'
                    }
                }
                with open(conf_file_path, 'w') as conf_file:
                    yaml.safe_dump(self._config, conf_file)

        return Config(self._config)

    @property
    def replacements(self):
        """
        Get the confsucker replacements file as a dict. Create the file if it doesn't exist
        :return: Replacements
        """
        if not self._replacements:
            replacements_file_path = os.path.join(self._conf_dir, 'replacements.yml')
            try:
                with open(replacements_file_path, 'r') as conf_stream:
                    self._replacements = yaml.safe_load(conf_stream)
            except FileNotFoundError:
                os.makedirs(self._conf_dir, exist_ok=True)
                self._replacements = {'default': {'example_var': 'local_value'}}
                with open(replacements_file_path, 'w') as replacements_file:
                    yaml.safe_dump(self._replacements, replacements_file)

        return Replacements(self._replacements)
