class Nooper:
    """
    Provide no-op push and pull. This is intended to allow use of a confhole which is not actively synchronised.
    """

    def pull(self):
        """
        No-op
        """

    def push(self):
        """
        No-op
        """
